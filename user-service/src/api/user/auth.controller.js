const jwt = require('jwt-simple')
const UserService = require('./user.service')
const { secret } = require('../../config');

module.exports = class AuthController {

  static async createUser(req, res) {
      return res.send(await UserService.save(req.body));
  };

  static async doLogin(req, res) {
    const dbUser = await UserService.fineOneByEmail(req.body.email);
    if (dbUser.validatePassword(req.body.password)) {
      const token = jwt.encode({ dbUser }, secret);
      return res.send({
        token,
        message: "bearer"
      });
    }
    return res.send("didn't find email or password");
  };

  // logout
  static async logout(req, res) {
    req.logout();
    res.send(res);
  };
}