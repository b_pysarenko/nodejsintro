const UserService = require('./user.service')

module.exports = class UserController {
  static async getUsers(req, res) {
    return res.send(await UserService.findAll());
  }
};
