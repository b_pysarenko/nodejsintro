const passport = require('passport')

module.exports = class Security {
  static bearer(req, res, next) {
    passport.authenticate('bearer', { session: false })(req, res, next)
  };
}