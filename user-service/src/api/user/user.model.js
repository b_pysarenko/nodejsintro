const crypto = require('crypto');
const mongoose = require('../../db/connection');

const { Schema } = mongoose;

const User = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String },
  username: { type: String, required: true },
  salt: { type: String, required: true }
});

User.methods.setPassword = function setPassword(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

User.methods.validatePassword = function validatePassword(password) {
  const pass = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.password === pass;
};

module.exports = mongoose.model('users', User);