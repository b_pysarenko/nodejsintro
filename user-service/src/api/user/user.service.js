const User = require('./user.model');

module.exports = class UserService {
    static async fineOneByEmail(email) {
        return User.findOne({ 'email': email });
    }

    static async findAll() {
        return User.find();
    }
    
    static async save(user) {
        const newUser = new User(user);
        newUser.setPassword(newUser.password);
        return newUser.save();
    }
}