const express = require('express');

const router = express.Router();
const userController = require('./users.controller');
const auth = require('./auth.controller');
const security = require('./middleware');
const ErrorHandler = require('../../error');

router.get('/users/', security.bearer, userController.getUsers);

router.post('/users/create', ErrorHandler.handle(auth.createUser));

router.post('/login', auth.doLogin);

router.get('/logout', auth.logout);


module.exports = router;