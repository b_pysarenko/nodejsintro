const BearerStrategy = require('passport-http-bearer');
const jwt = require('jwt-simple')
const { secret } = require('../config');
const UserService = require('../api/user/user.service')
const logger = require('../config/logger');

class CustomBearerStrategy {
    static async getStrategy(token, done) {
        try {
            const { dbUser } = jwt.decode(token, secret);
            const user = UserService.fineOneByEmail(dbUser.email);
            if (user) {
                return done(null, user)
            }
        } catch (error) {
            logger.error(error)
        }
        return done(null, false)
    }
}
module.exports = new BearerStrategy(CustomBearerStrategy.getStrategy);