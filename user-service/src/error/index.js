class ErrorHandler {
    static handle(handler) {
        return async (req, res, next) => {
            try {
                await handler(req, res);
            } catch (error) {
                res.status(500).send(error);
                next(error);
            }
        }
    };
}
module.exports = ErrorHandler