const mongoose = require('mongoose');
const { mongo } = require('../config');
const logger = require('../config/logger');

mongoose.connect(mongo.url);

const db = mongoose.connection;

db.once('open', () => logger.info('Connection ok!'));

module.exports = mongoose;