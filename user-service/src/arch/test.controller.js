const TestService=require('./test.service')

class TestController{
    static async testPromise(req, res){
        res.send(await TestService.succesPromise());
    } 

}

module.exports=TestController