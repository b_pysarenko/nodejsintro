const CustomPromise=require('./test.promise')

class TestService{

    static async testAsyncFunction(status) {
        return new CustomPromise((resolve, reject)=>{
           if (status==='resolve') {              
              return resolve('State is true');
           } 
           return reject(Error("State is false")); 
        });
     }

    static async succesPromise(){
        return this.testAsyncFunction('resolve');

    }
}
module.exports=TestService;