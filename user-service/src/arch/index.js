const express = require('express');
const TestController = require('./test.controller');
const ErrorHandler = require('../error');

const router = express.Router();

router.get('/test', ErrorHandler.handle(TestController.testPromise));

module.exports = router;