class CustomPromise {
    constructor(callback) {
        this.state = 'pending';
        this.value = null;
        const _reject = reason => {
            this.state = 'rejected';
            this.value = reason;

            if (this.deferred) {
                this.handle(this.deferred);
            }
        };

        const _resolve = newValue => {
            if (newValue && typeof newValue.then === 'function') {
                newValue.then(this.resolve, this.reject);
                return;
            }
            this.state = 'resolved';
            this.value = newValue;
            if (this.deferred) {
                this.handle(this.deferred);
            }
        };

        callback(_resolve, _reject);
    }

    handle(handler) {
        if (this.state === 'pending') {
            this.deferred = handler;
            return;
        }
        if (this.state === 'resolved') {
            this.handlerCallback = handler.onResolved;
        } else {
            this.handlerCallback = handler.onRejected;
        }
        if (!this.handlerCallback) {
            if (this.state === 'resolved') {
                handler.resolve(this.value);
            } else {
                handler.reject(this.value);

            }

            return;
        }
        const ret = this.handlerCallback(this.value);

        handler.resolve(ret);
    }

    then(onResolved, onRejected) {
        return new CustomPromise((resolve, reject) => {
            this.handle({
                onResolved,
                onRejected,
                resolve,
                reject
            });
        });
    };

}

module.exports = CustomPromise;