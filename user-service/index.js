const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const passport = require('passport')
const customBearerStrategy = require('./src/auth/bearer.strategy')
const usersRouter = require('./src/api/user');
const testRouter = require('./src/arch');
const { port } = require('./src/config');
const logger = require('./src/config/logger');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', usersRouter);
app.use('/', testRouter);
passport.use(customBearerStrategy);

// catch 404 and forward to error handler
app.use(function notFoundError(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function errorHandler(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  logger.error(err);
  // render the error page
  res.status(err.status || 500);
  res.send(err.message);
});

app.listen(port, () => logger.info(`Service started on *: ${port}`));
