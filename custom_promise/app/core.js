const states = require('./states');

class P {
  constructor(executor) {
    if (typeof executor !== 'function') {
      throw new Error('Executor must be a function');
    }
    this._state = states.pending;
    this._value = null;
    this._callbacks = [];

    const resolve = res => {
      if (this._state !== states.pending) {
        return;
      }

      const thenable = res != null ? res.then : null;
      if (typeof thenable === 'function') {
        return thenable(resolve, reject);
      }

      this._state = states.fulfilled;
      this._value = res;

      for (const { onFulfilled } of this._callbacks) {
        onFulfilled(res);
      }

      return res;
    };

    const reject = err => {
      if (this._state !== states.pending) {
        return;
      }

      const thenable = err != null ? err.then : null;
      if (typeof thenable === 'function') {
        return thenable(resolve, reject);
      }

      this._state = states.rejected;
      this._value = err;
      for (const { onRejected } of this._callbacks) {
        onRejected(err);
      }
      return err;
    };

    try {
      executor(resolve, reject);
    } catch (err) {
      reject(err);
    }
  }

  then(onFulfilled, onRejected) {
    return new P((resolve, reject) => {
      const _thenFulfilled = res => {
        try {
          resolve(onFulfilled(res));
        } catch (err) {
          reject(err);
        }
      };

      const _thenRejected = err => {
        try {
          onRejected ? reject(onRejected(err)): reject(err);
        } catch (newErr) {
          reject(newErr);
        }
      };

      if (this._state === states.fulfilled) {
        _thenFulfilled(this._value);
      } else if (this._state === states.rejected) {
        _thenRejected(this._value);
      } else {
        this._callbacks.push({
          onFulfilled: _thenFulfilled,
          onRejected: _thenRejected
        });
      }
    });
  }

  catch(onRejected) {
    return this.then(null, onRejected);
  }

  static resolve(val) {
    return new P((resolve) => {
      resolve(val);
    });
  }

  static reject(err) {
    return new P((resolve, reject) => {
      reject(err);
    });
  }
}

module.exports = P;
