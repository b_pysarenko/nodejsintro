const C = require('./core');

const executor = (resolve, reject) => {
  let rnd = Math.random().toFixed(1);
  console.log(rnd);
  setTimeout(()=>{
    if (rnd > 0.5) resolve('win ' + rnd);
    else reject(new Error('lose ' + rnd));
  },0);
};

function fC() {
  return new C(executor);
}

function fP() {
  return new Promise(executor);
}

const pr = fC();
debugger
pr.then(data => {
    const str = `1.resolved ${data}`;
    console.log(str);
  return str;
})
  .then(data => {
    console.log(`${data} || 2.resolved `);
    throw new Error('this err was thrown in then');
  })
  .catch(err => console.log(`catched ${err}`));

C.resolve(9).then(data => {
  console.log(`static resolve of ${data}`);
});
C.reject(10)
  .then(
    data => {
      console.log(`tried pass ${data} in static method`);
    },
    data => {
      console.log(`caught static rejected ${data} in then`);
      return ++data;
    }
  )
  .catch(err => {
    console.log(`caught reject from then ${err}`);
  });
