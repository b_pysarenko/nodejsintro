const mongoose = require('mongoose');
const log = require('../config/logger');

async function initConnection(cfg) {
  await mongoose.connect(cfg.url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    user: cfg.user,
    pass: cfg.user
  });

  const db = mongoose.connection;
  db.once('open', () => log.info(`Open connection: ${db.host}`)).on(
    'error',
    error => log.error(error.message)
  );

  log.info(`Connected to: ${db.host}`);
}

module.exports = initConnection;
