const express = require('express');
const log = require('./config/logger');
const { env, server, db } = require('./config');
const route = require('./api/messages');
require('./db')(db);

const app = express();

app.use(express.json());
app.use('/api', route);
app.listen(server.port, () => log.info(`Service started on : ${server.port}`));
log.info(`Run in '${env}' environment`);
