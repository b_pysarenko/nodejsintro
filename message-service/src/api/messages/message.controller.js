const log = require('../../config/logger');
const messageService = require('./message.service');

class MessageController {
  static getAll(req, res) {
    messageService
      .getAllMessages()
      .then(messages => res.send(messages))
      .catch(err => {
        log.error(err.message);
        res.status(500).json({ message: err.message });
      });
  }

  static createMessage(req, res) {
    messageService
      .saveNewMessage(req.body)
      .then(result => res.send(result))
      .catch(err => {
        log.error(err.message);
        res.status(400).json({ message: err.message });
      });
  }

  static getMessage(req, res) {
    messageService
      .findOneMessageById(req.params.id)
      .then(result => res.send(result))
      .catch(err => {
        log.error(err.message);
        res.status(400).json({ message: err.message });
      });
  }

  static updateMessage(req, res) {
    messageService
      .updateMessageById(req.params.id, req.body)
      .then(result => res.send(result))
      .catch(err => {
        log.error(err.message);
        res.status(400).json({ message: err.message });
      });
  }

  static deleteMessage(req, res) {
    messageService
      .deleteMessageById(req.id)
      .then(result => res.send(result))
      .catch(err => {
        log.error(err.message);
        res.status(400).json({ message: err.message });
      });
  }
}

module.exports = MessageController;
