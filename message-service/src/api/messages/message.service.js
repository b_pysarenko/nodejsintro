const Message = require('./message.model');

module.exports = {
  getAllMessages() {
    return Message.find({});
  },

  saveNewMessage(json) {
    return Message(json).save({});
  },

  findOneMessageById(id) {
    return Message.findOne({ _id: id });
  },

  updateMessageById(id, json) {
    return Message.findOneAndUpdate({ _id: id }, json, {
      upsert: true,
      useFindAndModify: true
    });
  },

  deleteMessageById(id) {
    return Message.deleteOne({ _id: id });
  }
};
