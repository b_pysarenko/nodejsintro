const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    user: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: true
    },
    createTime: {
      type: Date,
      required: true,
      default: Date.now
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model('Message', schema);