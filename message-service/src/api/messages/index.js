const router = require('express').Router();
const controller = require('./message.controller.js');

router
  .route('/')
  .get(controller.getAll)
  .post(controller.createMessage);
router
  .route('/:id')
  .get(controller.getMessage)
  .post(controller.updateMessage)
  .delete(controller.deleteMessage);

module.exports = router;
