module.exports = {
  env: process.env.NODE_ENV || 'local',
  server: {
    port: process.env.PORT || 3000
  },
  db: {
    url: 'mongodb://localhost:27017/SandboxMsg',
    user: 'messageService'
  }
};
