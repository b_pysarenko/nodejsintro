require('./testHelpers');
const { assert, expect } = require('chai');
const log = require('../src/config/logger');
const service = require('../src/api/messages/message.service');
const Message = require('../src/api/messages/message.model');

describe('Message service', () => {
  const msgs = [
    {
      user: 'Anon',
      message: 'Chat message one'
    },
    {
      user: 'Noname',
      message: 'Chat message two'
    }
  ];
  describe('Save new message', () => {
    it('save new message in repository', done => {
      service
        .saveNewMessage(msgs[0])
        .then(result => {
          const { user, message } = result;
          expect({ user, message }).to.deep.equal(msgs[0]);
          assert(result.id);
          assert(result.createTime);
        })
        .catch(err => {
          assert.fail('expected', 'actual', err);
          // assert.fail('actual', err);
        })
        .finally(done);
    });
  });
  describe('Get all messages', done => {
    it('return all messages from repository', async () => {
      await Message(msgs[0]).save({});
      await Message(msgs[1]).save({});
      service
        .getAllMessages()
        .then(result => {
          expect(result.length).to.equal(msgs.length);
        })
        .catch(err => {
          assert.fail('expected', 'actual', err);
          // assert.fail('actual', err);
        })
        .finally(done);
    });
  });
});
