const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/SandboxTest', {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
mongoose.connection
  .once('open', () => console.log('Connected!'))
  .on('error', error => {
    console.warn('Error : ', error);
  });

beforeEach(done => {
  mongoose.connection.collections.messages.drop(() => {
    done();
  });
});

after(done => {
  mongoose.disconnect(() => {
    done();
  });
});
